import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DestinoViaje } from 'src/app/models/DestinoViaje.model';
import { DestinosApiClient } from 'src/app/models/DestinoApiCliente.model'; 


@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers:[DestinosApiClient]
})
export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;
  constructor(private router:ActivatedRoute, private destinosApiClient: DestinosApiClient) { }

  ngOnInit(): void {
    // traer id desde url 
    let id = this.router.snapshot.paramMap.get('id'); 
    this.destino=this.destinosApiClient.getById(id);
  }
}
