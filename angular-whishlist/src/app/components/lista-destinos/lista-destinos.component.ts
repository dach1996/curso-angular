import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinoViaje } from '../../models/DestinoViaje.model';
import { DestinosApiClient } from '../../models/DestinoApiCliente.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})

export class ListaDestinosComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  update: string[];
  all;
  //destinos: DestinoViaje[];
  constructor(
    public destinoApiClient: DestinosApiClient,
    private store: Store<AppState>
  ) {
    //this.destinos = [];
    this.onItemAdded = new EventEmitter();
    this.update = [];

    this.store.select(state => state.destinos.favorito)
      .subscribe(data => {
        const d = data;
        if (d != null) {
          this.update.push('Se eligió: ' + d.nombre);
        }
      });
    this.all =store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }
  ngOnInit(): void {
    this.store.select(state => state.destinos)
      .subscribe(data => {
        let d = data.favorito;
        if (d != null) {
          this.update.push("Se eligió: " + d.nombre);
        }
      });

  }

  agregado(d: DestinoViaje) {
    this.destinoApiClient.add(d);
    this.onItemAdded.emit(d);
  }
  elegido(e: DestinoViaje) {
    this.destinoApiClient.elegir(e);
  }
}
