import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-vuelos-detalle-component',
  templateUrl: './vuelos-detalle-component.component.html',
  styleUrls: ['./vuelos-detalle-component.component.css']
})
export class VuelosDetalleComponentComponent implements OnInit {
  id: any; 
  constructor(private router: ActivatedRoute) /*propiedad necesaria para obtener params de url */ { 
    // se subscribe a los parametros pasados por la url y si viene uno llamado id, reemplaza la variable id por el valor de ese param
    router.params.subscribe(params => this.id = params['id']);
  }
  
  ngOnInit(): void {
  }

}